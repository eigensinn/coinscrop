/***************************************************************
 * Name:      coinscropMain.h
 * Purpose:   Defines Application Frame
 * Author:     ()
 * Created:   2017-03-29
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef COINSCROPMAIN_H
#define COINSCROPMAIN_H

#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>
#include <thread>
#include <Windows.h>
#include <codecvt>
#include <sstream>
#include "dirent.h"
#include "opencv2/opencv.hpp"
#include "prettyprint.hpp"
#include "wxthread.h"
using namespace std;

//(*Headers(coinscropFrame)
#include <wx/button.h>
#include <wx/checkbox.h>
#include <wx/frame.h>
#include <wx/gauge.h>
#include <wx/gbsizer.h>
#include <wx/menu.h>
#include <wx/radiobut.h>
#include <wx/statbmp.h>
#include <wx/stattext.h>
#include <wx/statusbr.h>
#include <wx/textctrl.h>
//*)

class coinscropFrame: public wxFrame
{
    //wxFrame* m_frame;
    public:

        coinscropFrame(wxWindow* parent,wxWindowID id = -1);
        virtual ~coinscropFrame();

    private:

        //(*Handlers(coinscropFrame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnButton1Click(wxCommandEvent& event);
        void OnButton2Click(wxCommandEvent& event);
        void OnCheckBox1Click(wxCommandEvent& event);
        //*)

        //(*Identifiers(coinscropFrame)
        static const long ID_STATICBITMAP1;
        static const long ID_TEXTCTRL2;
        static const long ID_BUTTON2;
        static const long ID_TEXTCTRL3;
        static const long ID_CHECKBOX1;
        static const long ID_TEXTCTRL4;
        static const long ID_TEXTCTRL5;
        static const long ID_STATICTEXT1;
        static const long ID_STATICTEXT2;
        static const long ID_RADIOBUTTON1;
        static const long ID_RADIOBUTTON2;
        static const long ID_GAUGE1;
        static const long idMenuQuit;
        static const long idMenuAbout;
        static const long ID_STATUSBAR1;
        //*)

        //(*Declarations(coinscropFrame)
        wxButton* Button2;
        wxCheckBox* CheckBox1;
        wxGauge* Gauge1;
        wxRadioButton* RadioButton1;
        wxRadioButton* RadioButton2;
        wxStaticBitmap* StaticBitmap1;
        wxStaticText* StaticText1;
        wxStaticText* StaticText2;
        wxStatusBar* StatusBar1;
        wxTextCtrl* TextCtrl2;
        wxTextCtrl* TextCtrl3;
        wxTextCtrl* TextCtrl4;
        wxTextCtrl* TextCtrl5;
        //*)

        wxMutex m_Mutex;
        bool m_ExitFlag;
        //CounterThread* m_CounterThreads[ncores];
        vector<CounterThread*> m_CounterThreads;

        vector<int> setSizes(cv::Mat &img, int &sb1_width, int &sb1_height);
        vector<string> getFilesNames(string folder);
        //void processing(string p, string name);
        string ws2s(const std::wstring& wstr);
        vector<string> split(string &s, string delimiter);
        string join(vector<string> s, string delimiter);
        wchar_t* convertCharArrayToLPCWSTR(const char* charArray);
        void matInfo(cv::Mat &M);
        void displayBitmap(wxCommandEvent& evt);

        DECLARE_EVENT_TABLE();
};

#endif // COINSCROPMAIN_H

/***************************************************************
 * Name:      coinscropApp.h
 * Purpose:   Defines Application Class
 * Author:     ()
 * Created:   2017-03-29
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef COINSCROPAPP_H
#define COINSCROPAPP_H

#include <wx/app.h>

class coinscropApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // COINSCROPAPP_H

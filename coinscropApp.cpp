/***************************************************************
 * Name:      coinscropApp.cpp
 * Purpose:   Code for Application Class
 * Author:     ()
 * Created:   2017-03-29
 * Copyright:  ()
 * License:
 **************************************************************/

#include "coinscropApp.h"

//(*AppHeaders
#include "coinscropMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(coinscropApp);

bool coinscropApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	coinscropFrame* Frame = new coinscropFrame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)
    return wxsOK;

}

#ifndef COUNTERTHREAD_H
#define COUNTERTHREAD_H

#include "wx/wx.h"
#include <ctime>
#include "opencv2/opencv.hpp"
//#define CANNY_THRESH_1 10
//#define CANNY_THRESH_2 200
#define MASK_DILATE_ITER 10
#define MASK_ERODE_ITER 10
//#define MASK_COLOR cv::Scalar(255,255,0)

using namespace std;

BEGIN_DECLARE_EVENT_TYPES()
    DECLARE_LOCAL_EVENT_TYPE(wxEVT_COUNTERTHREAD, -1)
END_DECLARE_EVENT_TYPES()

class CounterThread: public wxThread {
    //wxFrame* m_parent;
    private:
        //unsigned int m_cnt; // наш счетчик
        wxString time_s;
        wxMutex* m_mtx; // мьютекс
        bool* m_exitFlag; // указатель на флаг

        string m_p;
        string m_fn;
        string m_mode;
        bool m_blur;
        int m_blur_size;
        int m_canny_thresh_1;
        int m_canny_thresh_2;

        // закрытый метод для чтения флага
        bool GetExitFlag() {
            bool exitFlag;
            m_mtx->Lock();
            exitFlag = *m_exitFlag;
            m_mtx->Unlock();
            return exitFlag;
        }
    public:
        CounterThread(wxEvtHandler* pParent, wxThreadKind kind = wxTHREAD_DETACHED);
        //CounterThread(wxFrame* parent, wxThreadKind kind = wxTHREAD_DETACHED);
        // инициализируем класс
        void Create(wxMutex* mtx, bool* exitFlag, string& p, string& fn, string& mode, bool& blur, int& blur_size, int& canny_thresh_1, int& canny_thresh_2);
        // получаем значение счетчика (ПОСЛЕ остановки потока!)
        wxString GetCounter();
        // полезная нагрузка
        virtual void* Entry();
    protected:
        wxEvtHandler* m_pParent;
};


#endif

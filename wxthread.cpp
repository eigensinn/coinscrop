#include "coinscropApp.h"
#include "coinscropMain.h"
#include "wxthread.h"
#include <wx/msgdlg.h>

DEFINE_LOCAL_EVENT_TYPE(wxEVT_COUNTERTHREAD)
//CounterThread::CounterThread(wxFrame* parent, wxThreadKind kind):
CounterThread::CounterThread(wxEvtHandler* pParent, wxThreadKind kind):
            wxThread(kind), m_pParent(pParent) {
    //m_parent = parent;
            // do nothing
}

wxString CounterThread::GetCounter()
{
    return time_s;
}

void CounterThread::Create(wxMutex* mtx, bool* exitFlag, string& p, string& fn, string& mode, bool& blur, int& blur_size, int& canny_thresh_1, int& canny_thresh_2)
{
    //m_cnt = 0;
    m_mtx = mtx;
    m_exitFlag = exitFlag;
    m_p = p;
    m_fn = fn;
    m_mode = mode;
    m_blur = blur;
    m_blur_size = blur_size;
    m_canny_thresh_1 = canny_thresh_1;
    m_canny_thresh_2 = canny_thresh_2;
    wxThread::Create();
}

void* CounterThread::Entry()
{
    while(this->GetExitFlag() != true) {
        clock_t begin_t = clock();
        //string p = "C:\\workspace\\coinscrop";
        //string name = "117_6944";
        cv::Mat img = cv::imread(m_p+"\\in\\"+m_fn+".JPG");
        if(!img.empty()) {
            cv::Mat gray;
            cv::cvtColor(img, gray, cv::COLOR_BGR2GRAY);
            cv::Mat edges;
            cv::Canny(gray, edges, m_canny_thresh_1, m_canny_thresh_2);
            cv::dilate(edges, edges, cv::Mat());
            cv::erode(edges, edges, cv::Mat());

            map<float, vector<cv::Point> > contour_info;
            vector< vector<cv::Point> > contours;
            cv::findContours(edges, contours, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);
            for(auto i : contours) {
                // ������� ���� ������� ������� -> ������
                contour_info[cv::contourArea(i)] = i;
            }
            // ���������� ������������ ������ �� ��� ������� ���������
            float contour_area = 0;
            vector<cv::Point> max_contour;
            for(auto it = contour_info.cbegin(); it != contour_info.cend(); ++it) {
                if (it->first > contour_area) {
                    contour_area = it->first;
                    max_contour = it->second;
                }
            }

            //��������� ����� cout � textCtrl
            //wxStreamToTextRedirector redirect(TextCtrl2);

            //bool is_contour_convex = cv::isContourConvex(max_contour);
            cv::Mat mask = cv::Mat::zeros(edges.size().height, edges.size().width, CV_8UC1);

            if (m_mode == "-e") {
                cv::RotatedRect ellipse = cv::fitEllipse(max_contour);
                cv::ellipse(mask, ellipse, cv::Scalar(255,255,255), -1);
            } else {
                cv::fillConvexPoly(mask, max_contour, cv::Scalar(255,255,255));
            }

            cv::dilate(mask, mask, cv::Mat(), cv::Point(-1, -1), MASK_DILATE_ITER);
            cv::erode(mask, mask, cv::Mat(), cv::Point(-1, -1), MASK_ERODE_ITER);
            if (m_blur == true) {
                cv::blur(mask, mask, cv::Size(1, m_blur_size), cv::Point(-1,-1));
            }

            //�������� ������������� �������� �����
            cv::Mat neg;
            cv::bitwise_not(mask, neg);
            neg.setTo(cv::Scalar(255,255,0),neg);
            //cout << neg.at<uchar>(0,0) << endl;
            neg.convertTo(neg, CV_32FC1, 1/255.0);
            //cout << neg.at<cv::Vec3f>(0,0) << endl;
            //matInfo(neg);

            // �������� ������������� ��������������� �����
            cv::Mat neg_3[3] = {neg, neg, neg};
            cv::Mat Nfc3;
            cv::merge(neg_3, 3, Nfc3);

            // ������� ����������� � ����� � ������ float
            cv::Mat imgf;
            cv::Mat maskf;
            img.convertTo(imgf, CV_32FC3, 1/255.0);
            mask.convertTo(maskf, CV_32FC1, 1/255.0);
            //cv::Mat Dt = imgf(cv::Range(0,10), cv::Range(0,10));
            //cout << Dt << endl;

            // �������� ������������� �����
            cv::Mat mask_3[3] = {maskf, maskf, maskf};
            cv::Mat Afc3;
            cv::merge(mask_3, 3, Afc3);


            // ������������ ��������� ������������� ����� � ������������ float + ��������������� ������������� �����
            //cv::Mat masked = Afc3.mul(imgf) + Nfc3;
            // ������� ������������� � uchar
            //masked.convertTo(masked, CV_8UC3, 255.0);
            // ���������� �� ������
            vector<cv::Mat> channels;
            cv::split(imgf, channels);
            channels.push_back(maskf);
            cv::Mat img_a;
            cv::merge(channels, img_a);

            // �����������
            //wxImage aswxImage(masked.cols, masked.rows, masked.data, true);
            //wxSize sb1_size = StaticBitmap1->GetSize();
            //int sb1_height = sb1_size.GetHeight();
            //int sb1_width = sb1_size.GetWidth();
            //vector<int> sizes = setSizes(img, sb1_width, sb1_height);
            //aswxImage.Rescale(sizes.at(0), sizes.at(1));
            //wxBitmap bitmap = wxBitmap(aswxImage);
            //StaticBitmap1->SetBitmap(bitmap);

            //cv::Mat grayOutput, rgbOutput;
            //img_a.convertTo(grayOutput, CV_8U);
            //cv::cvtColor(grayOutput, rgbOutput, cv::COLOR_GRAY2BGR);
            //wxImage outputImg(rgbOutput.cols, rgbOutput.rows,  rgbOutput.data, true);
            //wxBitmap bitmap = wxBitmap(outputImg);

            //������
            cv::imwrite(m_p+"out\\"+m_fn+".png", img_a*255);

            string text = m_p+"out\\"+m_fn+".png";
            wxCommandEvent evt(wxEVT_COUNTERTHREAD, GetId());
            evt.SetString(text);
            //evt.SetClientData(&text);
            wxPostEvent(m_pParent, evt);

            clock_t end_t = clock();
            double elapsed_secs = double(end_t - begin_t) / CLOCKS_PER_SEC;
            time_s = wxString::Format(wxT("%f"), elapsed_secs);
            //wxLogMessage(time_s);
        }
    }
    return 0;
}

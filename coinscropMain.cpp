/***************************************************************
 * Name:      coinscropMain.cpp
 * Purpose:   Code for Application Frame
 * Author:     ()
 * Created:   2017-03-29
 * Copyright:  ()
 * License:
 **************************************************************/

#include "coinscropMain.h"
#include <wx/msgdlg.h>
#include <wx/wxprec.h>
#include <wx/utils.h>

//(*InternalHeaders(coinscropFrame)
#include <wx/intl.h>
#include <wx/string.h>
//*)

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//(*IdInit(coinscropFrame)
const long coinscropFrame::ID_STATICBITMAP1 = wxNewId();
const long coinscropFrame::ID_TEXTCTRL2 = wxNewId();
const long coinscropFrame::ID_BUTTON2 = wxNewId();
const long coinscropFrame::ID_TEXTCTRL3 = wxNewId();
const long coinscropFrame::ID_CHECKBOX1 = wxNewId();
const long coinscropFrame::ID_TEXTCTRL4 = wxNewId();
const long coinscropFrame::ID_TEXTCTRL5 = wxNewId();
const long coinscropFrame::ID_STATICTEXT1 = wxNewId();
const long coinscropFrame::ID_STATICTEXT2 = wxNewId();
const long coinscropFrame::ID_RADIOBUTTON1 = wxNewId();
const long coinscropFrame::ID_RADIOBUTTON2 = wxNewId();
const long coinscropFrame::ID_GAUGE1 = wxNewId();
const long coinscropFrame::idMenuQuit = wxNewId();
const long coinscropFrame::idMenuAbout = wxNewId();
const long coinscropFrame::ID_STATUSBAR1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(coinscropFrame,wxFrame)
    //(*EventTable(coinscropFrame)
    //*)
EVT_COMMAND(wxID_ANY, wxEVT_COUNTERTHREAD, coinscropFrame::displayBitmap)
END_EVENT_TABLE()

coinscropFrame::coinscropFrame(wxWindow* parent,wxWindowID id)
{
    //(*Initialize(coinscropFrame)
    wxGridBagSizer* GridBagSizer1;
    wxMenu* Menu1;
    wxMenu* Menu2;
    wxMenuBar* MenuBar1;
    wxMenuItem* MenuItem1;
    wxMenuItem* MenuItem2;

    Create(parent, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("wxID_ANY"));
    SetClientSize(wxSize(611,479));
    GridBagSizer1 = new wxGridBagSizer(0, 0);
    StaticBitmap1 = new wxStaticBitmap(this, ID_STATICBITMAP1, wxNullBitmap, wxDefaultPosition, wxSize(610,345), wxSIMPLE_BORDER, _T("ID_STATICBITMAP1"));
    GridBagSizer1->Add(StaticBitmap1, wxGBPosition(0, 0), wxGBSpan(1, 5), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    TextCtrl2 = new wxTextCtrl(this, ID_TEXTCTRL2, wxEmptyString, wxDefaultPosition, wxSize(110,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL2"));
    GridBagSizer1->Add(TextCtrl2, wxGBPosition(1, 1), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    Button2 = new wxButton(this, ID_BUTTON2, _("Run"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON2"));
    GridBagSizer1->Add(Button2, wxGBPosition(1, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    TextCtrl3 = new wxTextCtrl(this, ID_TEXTCTRL3, _("24"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL3"));
    GridBagSizer1->Add(TextCtrl3, wxGBPosition(1, 2), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    CheckBox1 = new wxCheckBox(this, ID_CHECKBOX1, _("Blur"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_CHECKBOX1"));
    CheckBox1->SetValue(false);
    GridBagSizer1->Add(CheckBox1, wxGBPosition(2, 2), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    TextCtrl4 = new wxTextCtrl(this, ID_TEXTCTRL4, _("10"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL4"));
    GridBagSizer1->Add(TextCtrl4, wxGBPosition(1, 3), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    TextCtrl5 = new wxTextCtrl(this, ID_TEXTCTRL5, _("200"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL5"));
    GridBagSizer1->Add(TextCtrl5, wxGBPosition(1, 4), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("CANNY_THRESH_1"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
    GridBagSizer1->Add(StaticText1, wxGBPosition(2, 3), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText2 = new wxStaticText(this, ID_STATICTEXT2, _("CANNY THRESH_2"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
    GridBagSizer1->Add(StaticText2, wxGBPosition(2, 4), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    RadioButton1 = new wxRadioButton(this, ID_RADIOBUTTON1, _("Ellipse"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_RADIOBUTTON1"));
    RadioButton1->SetValue(true);
    GridBagSizer1->Add(RadioButton1, wxGBPosition(2, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    RadioButton2 = new wxRadioButton(this, ID_RADIOBUTTON2, _("Polynom"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_RADIOBUTTON2"));
    GridBagSizer1->Add(RadioButton2, wxGBPosition(2, 1), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    Gauge1 = new wxGauge(this, ID_GAUGE1, 100, wxDefaultPosition, wxSize(584,28), 0, wxDefaultValidator, _T("ID_GAUGE1"));
    GridBagSizer1->Add(Gauge1, wxGBPosition(3, 0), wxGBSpan(1, 5), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    SetSizer(GridBagSizer1);
    MenuBar1 = new wxMenuBar();
    Menu1 = new wxMenu();
    MenuItem1 = new wxMenuItem(Menu1, idMenuQuit, _("Quit\tAlt-F4"), _("Quit the application"), wxITEM_NORMAL);
    Menu1->Append(MenuItem1);
    MenuBar1->Append(Menu1, _("&File"));
    Menu2 = new wxMenu();
    MenuItem2 = new wxMenuItem(Menu2, idMenuAbout, _("About\tF1"), _("Show info about this application"), wxITEM_NORMAL);
    Menu2->Append(MenuItem2);
    MenuBar1->Append(Menu2, _("Help"));
    SetMenuBar(MenuBar1);
    StatusBar1 = new wxStatusBar(this, ID_STATUSBAR1, 0, _T("ID_STATUSBAR1"));
    int __wxStatusBarWidths_1[1] = { -1 };
    int __wxStatusBarStyles_1[1] = { wxSB_NORMAL };
    StatusBar1->SetFieldsCount(1,__wxStatusBarWidths_1);
    StatusBar1->SetStatusStyles(1,__wxStatusBarStyles_1);
    SetStatusBar(StatusBar1);
    SetSizer(GridBagSizer1);
    Layout();

    Connect(ID_BUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&coinscropFrame::OnButton2Click);
    Connect(idMenuQuit,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&coinscropFrame::OnQuit);
    Connect(idMenuAbout,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&coinscropFrame::OnAbout);
    //*)
}

coinscropFrame::~coinscropFrame()
{
    //(*Destroy(coinscropFrame)
    //*)
}

void coinscropFrame::OnQuit(wxCommandEvent& event)
{
    Close();
}

void coinscropFrame::OnAbout(wxCommandEvent& event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}

wchar_t* coinscropFrame::convertCharArrayToLPCWSTR(const char* charArray)
{
    wchar_t* wString=new wchar_t[4096];
    MultiByteToWideChar(CP_ACP, 0, charArray, -1, wString, 4096);
    return wString;
}

string coinscropFrame::ws2s(const std::wstring& wstr)
{
    //wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
    //std::string u8str = converter.to_bytes(wstr);
    //return u8str;
    using convert_typeX = std::codecvt_utf8<wchar_t>;
    wstring_convert<convert_typeX, wchar_t> converterX;
    return converterX.to_bytes(wstr);

}

vector<string> coinscropFrame::getFilesNames(string folder)
{
    wxStreamToTextRedirector redirect(TextCtrl2);
    /*vector<string> names;
    string search_path = folder + "*.*";
    WIN32_FIND_DATA fd;
    HANDLE hFind = ::FindFirstFile(convertCharArrayToLPCWSTR(search_path.c_str()), &fd);
    if(hFind != INVALID_HANDLE_VALUE) {
        do {
            // read all (real) files in current folder
            // , delete '!' read other 2 default folder . and ..
            if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) {
                wstring ws(fd.cFileName);
                string name(ws.begin(), ws.end());
                name.erase(name.end()-4, name.end());
                names.push_back(name);
            }
        }while(::FindNextFile(hFind, &fd));
        ::FindClose(hFind);
    }
    return names;*/
    wstring w_folder = wstring(folder.begin(), folder.end());
    vector<string> names;
    WDIR *dir;
    struct wdirent *ent;
    if ((dir = wopendir (w_folder.c_str())) != NULL) {
      /* print all the files and directories within directory */
        while ((ent = wreaddir (dir)) != NULL) {
            //printf ("%s\n", ent->d_name);
            //names.push_back(ent->d_name);
            wstring ws(ent->d_name);
            //string str(ws.begin(), ws.end());
            string name = ws2s(ent->d_name);
            string before_dot = split(name, ".").at(0);
            if (before_dot != "") {
                names.push_back(before_dot);
            }
        }
        wclosedir (dir);
    }
    return names;
}

void coinscropFrame::matInfo(cv::Mat& M)
{
    cout << "M.dims = " << M.dims << " M.size = [";
    for(int i = 0; i < M.dims; ++i) {
        if(i) cout << " x ";
        cout << M.size[i];
    }
    cout << "] M.channels = " << M.channels() << endl;
}

void coinscropFrame::OnButton1Click(wxCommandEvent& event)
{

}

vector<int> coinscropFrame::setSizes(cv::Mat &img, int &sb1_width, int &sb1_height)
{
    vector<int> sizes;
    if (img.rows > img.cols){
        double scale = (double)sb1_height/img.rows;
        int sb1_width = scale*img.cols;
        sizes = {sb1_width, sb1_height};
    } else {
        double scale = (double)sb1_width/img.cols;
        int sb1_height = scale*img.rows;
        sizes = {sb1_width, sb1_height};
    }
    return sizes;
}

string coinscropFrame::join(vector<string> s, string delimiter) {
    stringstream ss;
    copy(s.begin(),s.end(), ostream_iterator<string>(ss, delimiter.c_str()));
    return ss.str();
}

vector<string> coinscropFrame::split(string &s, string delimiter)
{
    size_t pos = 0;
    string token;
    vector<string> output;
    while ((pos = s.find(delimiter)) != string::npos) {
        token = s.substr(0, pos);
        //std::cout << token << std::endl;
        output.push_back(token);
        s.erase(0, pos + delimiter.length());
    }
    //std::cout << s << std::endl;
    return output;
}

void coinscropFrame::OnButton2Click(wxCommandEvent& event)
{
    wxStreamToTextRedirector redirect(TextCtrl2);
    // ����������� ���� ������������ �����
    HMODULE hModule = GetModuleHandleW(NULL);
    WCHAR path[MAX_PATH];
    GetModuleFileNameW(hModule, path, MAX_PATH);
    wstring ws(path);
    string p(ws.begin(), ws.end());
    // ������� �� ���� coinscrop.exe
    //p.erase(p.end()-13, p.end());
    vector<string> pv = split(p, "\\");
    //cout << pv << endl;
    p = join(pv, "\\");
    //cout << p << endl;
    // ���������� ����� ���� ������ � �����
    vector<string> filenames = getFilesNames(p + "in\\");
    //cout << filenames << endl;
    // ������� ��������� ��� �������
    vector<thread> threads;

    string mode;
    bool ellipse = RadioButton1->GetValue();
    if (ellipse) {
        mode = "-e";
    } else {
        mode = "-p";
    }
    bool blur = CheckBox1->GetValue();
    int blur_size = wxAtoi(TextCtrl3->GetValue());
    int canny_thresh_1 = wxAtoi(TextCtrl4->GetValue());
    int canny_thresh_2 = wxAtoi(TextCtrl5->GetValue());

    // ��������� ��������� �������
    /*for(auto i : filenames){
        threads.push_back(std::thread(processing, p, i, mode, blur, blur_size, canny_thresh_1, canny_thresh_2));
    }

    for(auto& t : threads){
        t.join();
    }*/



    // ����� �������
    //const int ThreadsNumber = sizeof(m_CounterThreads)/sizeof(m_CounterThreads[0]);
    //���������� ���������� ���� ����������
    unsigned int ncores = std::thread::hardware_concurrency();

    vector<string> c_filenames = filenames;

    vector<vector<string> > mass;
    while (filenames.size() >= ncores){
        vector<string>::const_iterator first = filenames.begin();
        vector<string>::const_iterator last = filenames.begin() + ncores;
        vector<string> newVec(first, last);
        mass.push_back(newVec);
        filenames.erase(filenames.begin(), filenames.begin() + ncores);
    }
    if (filenames.size() > 0) {
        mass.push_back(filenames);
    }

    /*for(auto const& n: mass) {
        for(auto const& v: n) {
            cout << v;
        }
        cout << endl;
    }*/

    int cnt = 0;
    for(unsigned int vec = 0; vec < mass.size(); vec++) {
        // ���� ���������� ������ �������
        m_ExitFlag = false;
        // ������� ������
        for(unsigned int i = 0; i < mass.at(vec).size(); i++) {
            try {
                CounterThread* th = new CounterThread(this, wxTHREAD_JOINABLE);
                m_CounterThreads.push_back(th);
            } catch(bad_alloc& error) {
                // �� ������� ������� ����� ��-�� �������� ������
                if(i > 0) {
                    for(unsigned int j = 0; j < i; j++) {
                    //for(auto const& j : m_CounterThreads){
                        //wxDELETE(j);
                        wxDELETE(m_CounterThreads.at(j));
                    }
                }
                wxMessageBox(_("Failed to start thread #") +
                             wxString::Format(_T("%d"), i));
                return;
            }
            m_CounterThreads.at(i)->Create(&m_Mutex, &m_ExitFlag, p, mass.at(vec).at(i), mode, blur, blur_size, canny_thresh_1, canny_thresh_2);
            cout << mass.at(vec).at(i) << endl;
        }

        for(unsigned int i = 0; i < mass.at(vec).size(); i++) {
            m_CounterThreads.at(i)->Run();
        }
        // ���� ���� �������
        wxMilliSleep(10);
        // ������������� ���� ������
        m_Mutex.Lock();
        m_ExitFlag = true;
        m_Mutex.Unlock();
        // ���� ����� ��������� �������� ���������
        //unsigned int counters[ThreadsNumber];
        // � ���� - ������������ �� ������� ���
        //wxString randomNumber;
        // ���� ���������� �������, ����������� ������
        for(unsigned int i = 0; i < mass.at(vec).size(); i++) {
            // ���� ����������
            m_CounterThreads.at(i)->Wait();
            // �������� �������� ��������
            //cout << mass.at(vec) << endl;
            // ����� ������ �� �����
            wxDELETE(m_CounterThreads.at(i));
            cnt += 1;
            Gauge1->SetValue(Gauge1->GetRange()*cnt/c_filenames.size());
            // ���������� � ������ �������� �������� ���� ��������
            //randomNumber += wxString::Format(_T("%d"), counters[i] & 1);
        }
        m_CounterThreads.clear();
        //m_CounterThreads.shrink_to_fit();
        // ������� ���������
        //ListBox1->Insert(randomNumber, 0);
    }
    Gauge1->SetValue(0);
}

void coinscropFrame::displayBitmap(wxCommandEvent& evt)
{
    //string* name = (string*)evt.GetClientData();
    //string value = *name;
    wxString text = evt.GetString();
    wxSize sb1_size = StaticBitmap1->GetSize();
    int sb1_width = sb1_size.GetWidth();
    int sb1_height = sb1_size.GetHeight();
    string path = string(text.mb_str());
    wxBitmap bitmap(wxImage(path).Rescale(sb1_width, sb1_height));
    StaticBitmap1->SetBitmap(bitmap);
}

void coinscropFrame::OnCheckBox1Click(wxCommandEvent& event)
{
}

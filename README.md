# CoinsCrop

## Приложение для удаления фона при съемке коллекционных монет

### Основные особенности

  * приложение основано на openCV 4.0 и WxWidgets 3.1;
  * для создания и сборки приложения использовался Code::Blocks 17.12 c WxSmith;
  * используются два метода создания контура: эллипс (```cv::fitEllipse```) или полином (```cv::fillConvexPoly```);
  * используется реализация потоков, подсказанная [здесь](http://qaru.site/questions/1311551/threads-in-wxwidgets).
  
### Сборка под Windows

  * в качестве компилятора при сборке использовался [MinGW-W64 GCC-8.1.0](https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win64/Personal%20Builds/mingw-builds/8.1.0/threads-posix/seh/) (posix/seh);
  * использовались [OpenCV-MinGW-Build](https://github.com/huihut/OpenCV-MinGW-Build) [OpenCV 4.0.1-x64](https://github.com/huihut/OpenCV-MinGW-Build/archive/OpenCV-4.0.1-x64.zip) для MinGW-x86_64-8.1.0-posix-seh-rt_v6-rev0;
  * использовалась [wxWidgets 3.2.1](https://www.wxwidgets.org/downloads/):
  
    * [заголовочные файлы](https://github.com/wxWidgets/wxWidgets/releases/download/v3.1.2/wxWidgets-3.1.2.zip)
	* [библиотеки (debug)](https://github.com/wxWidgets/wxWidgets/releases/download/v3.1.2/wxMSW-3.1.2_gcc810_x64_Dev.7z)
	* [библиотеки (relese)](https://github.com/wxWidgets/wxWidgets/releases/download/v3.1.2/wxMSW-3.1.2_gcc810_x64_ReleaseDLL.7z)
	
  * рекомендуется переместить разархивированные папки OpenCV-MinGW-Build-OpenCV-4.0.1-x64 и wxWidgets-3.1.2 в каталог с проектом. Собранные библиотеки wxWidgets (*.a и *.dll) разархивировать и переместить в wxWidgets-3.1.2\lib\gcc_dll и wxWidgets-3.1.2\lib\gcc_dll_release.
	
  * настраивать Code::Blocks 17.12 при первом использовании рекомендуется в соответствии с [этим руководством](https://habr.com/ru/post/212027/), выбирая в качестве пути существующий проект [..\coinscrop](https://bitbucket.org/eigensinn/coinscrop/src/master/). Подтвердить перезапись [coinscrop.cbp](https://bitbucket.org/eigensinn/coinscrop/src/master/coinscrop.cbp) и [coinscrop.depend](https://bitbucket.org/eigensinn/coinscrop/src/master/coinscrop.depend). Остальные файлы оставить без изменений.
  * Настройки для Project/Build options... можно сверить по [скриншотам](https://bitbucket.org/eigensinn/coinscrop/src/master/screenshots/).
  
### Скачать приложение (exe)

[Скачать coinscrop.exe](https://drive.google.com/open?id=1maDFPwlDw_5EquxjH9vYcUbs3NKMmZq2)

### Лицензия

[MIT License](https://bitbucket.org/eigensinn/coinscrop/src/master/LICENSE)